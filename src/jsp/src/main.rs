extern crate rand; // RNG
extern crate clap; // CLI args
extern crate rayon; // ||
extern crate random_choice;
extern crate time;
use time::PreciseTime;
use std::mem;
use rand::{thread_rng, Rng}; // RNG
use clap::{Arg, App}; // CLI args
use std::fs::File;
use std::io::prelude::*;
use self::random_choice::random_choice;
use rayon::prelude::*;

/// Problembeschreibung als Struktur von Time, Deadline und Penalty
struct Problem {
	zeiten: Vec<i64>,
	fristen: Vec<i64>,
	strafen: Vec<i64>,
}

// job sequencing with deadlines
fn main() {
    let matches = App::new("Job Shop with Penalties")
        .version("0.1")
        .author("Johannes Hamfler <johannes.hamfler@stud.htwk-leipzig.de>")
        .about("Job Shop with Penalties")
        .arg(Arg::with_name("Eingabedatei")
            .help("Eingabedatei angeben")
            .required(true))
        .arg(Arg::with_name("c")
            .short("c")
            .multiple(true)
            .help("legt den Kompaktheitsgrad fest"))
        .arg(Arg::with_name("maximal erlaubte akkumulierte Strafe")
             .short("K")
             .long("strafen")
             .help("legt die maximal erlaubte Strafe fest")
             .required(false)
             .takes_value(true)
             .validator(ist_ganzzahl)
             .default_value("4000"))
        .arg(Arg::with_name("Generationen")
            .short("g")
            .long("generationen")
            .help("legt die Anzahl der Generationen fest")
            .required(false)
            .takes_value(true)
            .validator(ist_ganzzahl)
            .default_value("5000"))
        .arg(Arg::with_name("Populationsgröße")
            .short("p")
            .long("populationsgröße")
            .help("legt die Populationsgröße fest")
            .required(false)
            .takes_value(true)
            .validator(ist_ganzzahl)
            .default_value("50"))
        .arg(Arg::with_name("Anzahl der Eltern")
            .short("e")
            .long("eltern")
            .help("legt die Anzahl der Eltern fest")
            .required(false)
            .takes_value(true)
            .validator(ist_ganzzahl)
            .default_value("10"))
        .arg(Arg::with_name("Anzahl der Kinder")
            .short("k")
            .long("kinder")
            .help("legt die Anzahl der Kinder fest")
            .required(false)
            .takes_value(true)
            .validator(ist_ganzzahl)
            .default_value("45")) // 45
        .arg(Arg::with_name("Mutationsart")
            .short("m")
            .long("mutation")
            .help("legt die verwendete Mutationsmethode fest")
            .required(false)
            .takes_value(true)
            .default_value("vertausche")
            .possible_values(&["vertausche", "invertiere", "verschiebe", "mische"]))
        .arg(Arg::with_name("Rekombinationsart")
            .short("r")
            .long("rekombination")
            .help("legt die verwendete Rekombinationsmethode fest")
            .required(false)
            .takes_value(true)
            .default_value("ein-punkt-crossover")
            .possible_values(&["ordnung", "kante", "uniformer-crossover", "zwei-punkt-crossover", "ein-punkt-crossover"]))
        .arg(Arg::with_name("Selektionsmethode")
            .short("s")
            .long("selektion")
            .help("legt die verwendete Eltern-Selektionsmethode fest")
            .required(false)
            .takes_value(true)
            .default_value("stochastisch")
            .possible_values(&["stochastisch", "fitnessproportional"]))
        .arg(Arg::with_name("Benchmark")
            .short("b")
            .long("benchmark")
            .help("führt das Programm im Benchmark-Modus aus")
            .required(false)
            .takes_value(true)
            .default_value("0")
            .possible_values(&["0", "1"]))
        .get_matches();

    let dateiname = matches.value_of("Eingabedatei").unwrap();
    let mut generationen = matches.value_of("Generationen").unwrap().parse::<usize>().unwrap();
    if generationen < 2 { generationen = 2; }

    let mutationsart = matches.value_of("Mutationsart").unwrap();
    let selektionsart = matches.value_of("Selektionsmethode").unwrap();
    // arithmetischer crossover kann nicht verwendet werden, da ¬∊ℝ^l
    let rekombinationsart = matches.value_of("Rekombinationsart").unwrap();
    let kinder = matches.value_of("Anzahl der Kinder").unwrap().parse::<usize>().unwrap();
    let eltern = matches.value_of("Anzahl der Eltern").unwrap().parse::<usize>().unwrap();
    let anzahl_individuen = matches.value_of("Populationsgröße").unwrap().parse::<usize>().unwrap();
    let maximale_strafe = matches.value_of("maximal erlaubte akkumulierte Strafe").unwrap().parse::<i64>().unwrap();
    let kompaktheitsgrad= matches.occurrences_of("c");
    let benchmark = matches.value_of("Benchmark").unwrap().parse::<usize>().unwrap();
	let problem = lese_problem(dateiname);
    let start = PreciseTime::now();
    bearbeite_problem(problem, maximale_strafe, anzahl_individuen, eltern, kinder, generationen, mutationsart, rekombinationsart, selektionsart, kompaktheitsgrad, benchmark);
    let end = PreciseTime::now();
    if benchmark == 2 { println!("ZEIT {}", start.to(end)); }
}

fn ist_ganzzahl(string : String) -> Result<(), String> {
    match string.parse::<i64>() {
        Ok(_) => Ok(()),
        Err(e) => Err(format!(" Es wurde kein ganzzahliger Typ übergeben. {}", e).to_string()),
    }
}

fn lese_problem(filename: &str) -> Problem {
    let mut f = File::open(filename).expect("file not found");
    let mut contents = String::new();
    f.read_to_string(&mut contents).expect("something went wrong reading the file");
    //println!("{}", contents);

    let lines = contents.split('\n');

    let vlines = lines.collect::<Vec<&str>>();
    let tvals = vlines[0].split(" ").collect::<Vec<&str>>(); // Ausführungszeiten
    let dvals = vlines[1].split(" ").collect::<Vec<&str>>(); // Fristen
    let pvals = vlines[2].split(" ").collect::<Vec<&str>>(); // Strafen

    let mut times : Vec<i64> = Vec::new();
    for time in tvals {
        times.push(match time.trim().parse() {
            Ok(num) => num,
            Err(_) => 0, // catchall
        });
    }

    let mut deadlines : Vec<i64> = Vec::new();
    for deadline in dvals {
        deadlines.push(match deadline.trim().parse() {
            Ok(num) => num,
            Err(_) => 0, // catchall
        });
    }

    let mut penalties : Vec<i64> = Vec::new();
    for penalty in pvals {
        penalties.push(match penalty.trim().parse() {
            Ok(num) => num,
            Err(_) => 0, // catchall
        });
    }

    if times.len() != deadlines.len() { println!("times.len != deadlines.len"); }
    if times.len() != penalties.len() { println!("times.len != penalties.len"); }

    // return it
    Problem { zeiten: times, fristen: deadlines, strafen: penalties }
}

fn bearbeite_problem(problem :Problem, k :i64, anzahl_individuen :usize, anzahl_eltern :usize, anzahl_kinder :usize, generationen :usize, mutationsart :&str, rekombinationsart :&str, selektionsart :&str, c :u64, benchmark :usize) {
    let mut individuen: Vec<Vec<usize>> = Vec::new();

    initialisiere_individuen(&problem, anzahl_individuen, &mut individuen);

    let mut bewertungen = bewerte_individuen(&problem, &individuen);
    let mut durchschnitt = durchschnittliche_bewertung(&bewertungen);
    let (mut bester_index, mut beste_bewertung) = finde_bestes_individuum(&bewertungen);
    let mut bestes_individuum :Vec<usize> = individuen[bester_index].to_vec();
    let mut anzahl_eltern = anzahl_eltern;
    let mut anzahl_kinder = anzahl_kinder;

    if anzahl_individuen > anzahl_eltern + anzahl_kinder {
        anzahl_eltern = anzahl_individuen / 2;
        if anzahl_eltern < 2 {
            anzahl_eltern = 2;
        }
        anzahl_kinder = anzahl_individuen; // - anzahl_eltern;
    }
    if anzahl_eltern > anzahl_individuen {
        anzahl_eltern = anzahl_individuen;
    }

    for g in 0..generationen {
        if benchmark == 0 {
            eprint!("Generation: {}\t Ø:{:.*}\r", g, 1, durchschnitt);
        }

        // Terminierungsbedingung -----------------
        if beste_bewertung <= k {
            if c < 4 {
                println!("Individuum mit Bewertung {} <= k {} gefunden", beste_bewertung, k);
                if c < 2 {
                    drucke_individuum(&bestes_individuum);
                }
                println!("Phenotyp");
                drucke_phaenotyp(&problem, &bestes_individuum);
            }
            return; // optimales Individuum wurde gefunden
        }


        // Elternselektion -----------------
        if c < 3 {
            let (_neuer_index, neue_bewertung) = finde_bestes_individuum(&bewertungen);
            println!("0. nb: {} g: {}, Ø: {}", neue_bewertung, g, durchschnittliche_bewertung(&bewertungen));
        }

        let mut eltern = selektiere_eltern(&mut individuen, &bewertungen, anzahl_eltern, selektionsart);

        // Richtungsvorgabe durch definitive Selektion des besten Individuums
        // nimmt Einfluss auf Rekombination und Turnierselektion
        eltern.push(bestes_individuum.to_vec());

        if c < 3 {
            let bewertungen = bewerte_individuen(&problem, &eltern);
            let (_neuer_index, neue_bewertung) = finde_bestes_individuum(&bewertungen);
            println!("1. nb: {} g: {}, Ø: {}", neue_bewertung, g, durchschnittliche_bewertung(&bewertungen));
        }
        if c < 1 {
            println!("Eltern:");
            drucke_individuen(&eltern);
        }

        // Rekombination -----------------
        // kombinierende rekombination erhält diversität
        // interpolierende rekombination verwischt population (gemeinsamer nenner) = genetisches Reparieren (als Gegenstück dann diversitätserhaltende Mutation verwenden)
        // extrapolierende rekombination arithmetischerXO der in richtung des besseren ind extrapoliert
        // ist beides nicht möglich da !€R^l

        // ordnungsrekombination evtl sinnvoll, da deadlines unterschiedlich
        // kantenrekombination vielleicht etwas sinnvoll, da nacheinanderausführung von zwei jobs deadlines verändert?
        // es müssen nicht alle neuen ind auf diesem weg erzeugt werden

        // führt eine Rekombination durch, um Kinder zu erzeugen
        let mut kinder: Vec<Vec<usize>> = rekombiniere(rekombinationsart, &mut eltern, anzahl_kinder);


        // Mutation -----------------
        // mutiere die Kinder
        if c < 1 { println!("k1  {:?}", kinder[0]); }
        kinder.par_iter_mut().map(|mut kind| mutiere(mutationsart, &mut kind, 0.3)).count();
        if c < 1 { println!("k1' {:?}", kinder[0]); }

        // füge Kinder und Eltern zusammen
        let mut individuen = eltern;

        individuen.append(&mut kinder);


        // Bewertung -----------------
        bewertungen = bewerte_individuen(&problem, &individuen);
        if c < 3 {
            let (_neuer_index, neue_bewertung) = finde_bestes_individuum(&bewertungen);
            println!("2. nb: {} g: {}, Ø: {}", neue_bewertung, g, durchschnittliche_bewertung(&bewertungen));
        }

        // Umweltselektion -----------------
        // besten-selektion, plus-selektion, komma-selektion, q-stufige-turnier-selektion

        // geordneter Vektor mit den besten Indexen aus dem Turnier zuerst
        let turnierindex = turnierselektion(&bewertungen, 4);

        if c < 1 {
            println!("tlen {}", turnierindex.len());
            println!("ilen {}", individuen.len());
            println!("blen {}", bewertungen.len());
        }

        // entferne schlechteste Individuen nach Umweltselektion
        let mut schlechteste_indexe: Vec<usize> = Vec::new();
        let len = individuen.len();

        if len >= anzahl_individuen {
            for i in anzahl_individuen..len {
                schlechteste_indexe.push(turnierindex[i]);
            }
        } else {
            if c < 1 {
                println!("sollte nicht möglich sein");
            }
            return;
        }

        let mut count = 0;

        schlechteste_indexe.par_sort();
        for s in schlechteste_indexe {
            individuen.remove(s - count);
            bewertungen.remove(s - count);
            count += 1;
        }

        // speichere bestes Individuum
        let (neuer_index, neue_bewertung) = finde_bestes_individuum(&bewertungen);

        if c < 4 || benchmark == 1 {
            durchschnitt = durchschnittliche_bewertung(&bewertungen);
            if neue_bewertung < beste_bewertung {
                beste_bewertung = neue_bewertung;
                bester_index = neuer_index;
                bestes_individuum = individuen[bester_index].to_vec();

                if benchmark == 1 {
                    println!("{:9} {:9} {:9.0}", beste_bewertung, g, durchschnitt);
                }
                if benchmark == 0 {
                    println!("Bewertung: {:9} Generation: {:9}  Ø: {:9.0}", beste_bewertung, g, durchschnitt);
                }
            }
        }
    }

    if benchmark == 0 {
        if c < 4 {
            println!("Bestes Individuum: {} > k {}", beste_bewertung, k);
            drucke_individuum(&bestes_individuum);
            println!("Phänotyp");
            drucke_phaenotyp(&problem, &bestes_individuum);
        }
    }
}

fn rekombiniere(art :&str, eltern :&mut Vec<Vec<usize>>, anzahl_kinder :usize) -> Vec<Vec<usize>> {
    let kinder :Vec<Vec<usize>>;
    match art {
        "ordnung" => kinder = ordnungsrekombination(eltern, anzahl_kinder),
        "zwei-punkt-crossover" => kinder = zwei_punkt_crossover(eltern, anzahl_kinder),
        "ein-punkt-crossover" => kinder = ein_punkt_crossover(eltern, anzahl_kinder),
        _ => kinder = ordnungsrekombination(eltern, anzahl_kinder),
    }
    kinder
}

// buggy
fn zwei_punkt_crossover(eltern :&mut Vec<Vec<usize>>, anzahl_kinder :usize) -> Vec<Vec<usize>> {
    let mut max = anzahl_kinder;
    let vlen = eltern[0].len();
    let elen = eltern.len();
    let mut kinder :Vec<Vec<usize>>= Vec::new();
    let mut rng = rand::thread_rng();

    if max % 2 == 0 {
        while max > 0 {
            let mut r1 = rng.gen_range(0, vlen);
            let mut r2 = rng.gen_range(0, vlen);
            let elter1 = &eltern[rng.gen_range(0, elen)];
            let elter2 = &eltern[rng.gen_range(0, elen)];
            let mut kind1 :Vec<usize> = Vec::new();
            let mut kind2 :Vec<usize> = Vec::new();

            if r1 > r2 {
                mem::swap(&mut r1, &mut r2);
            }

            kind1.extend(&elter1[0..r1]);
            kind1.extend(&elter2[r1..r2]);
            kind1.extend(&elter1[r2..vlen]);
            kind2.extend(&elter2[0..r1]);
            kind2.extend(&elter1[r1..r2]);
            kind2.extend(&elter2[r2..vlen]);
            kinder.push(kind1);
            kinder.push(kind2);
            max -= 2;
        }
    } else {
        while max != 1 {
            let mut r1 = rng.gen_range(0, vlen);
            let mut r2 = rng.gen_range(0, vlen);
            let elter1 = &eltern[rng.gen_range(0, elen)];
            let elter2 = &eltern[rng.gen_range(0, elen)];
            let mut kind1 :Vec<usize> = Vec::new();
            let mut kind2 :Vec<usize> = Vec::new();

            if r1 > r2 {
                mem::swap(&mut r1, &mut r2);
            }

            kind1.extend(&elter1[0..r1]);
            kind1.extend(&elter2[r1..r2]);
            kind1.extend(&elter1[r2..vlen]);
            kind2.extend(&elter2[0..r1]);
            kind2.extend(&elter1[r1..r2]);
            kind2.extend(&elter2[r2..vlen]);
            kinder.push(kind1);
            kinder.push(kind2);
            max -= 2;
        }

        let mut r1 = rng.gen_range(0, vlen);
        let mut r2 = rng.gen_range(0, vlen);
        let elter1 = &eltern[rng.gen_range(0, elen)];
        let elter2 = &eltern[rng.gen_range(0, elen)];
        let mut kind1 :Vec<usize> = Vec::new();

        if r1 > r2 {
            mem::swap(&mut r1, &mut r2);
        }

        kind1.extend(&elter1[0..r1]);
        kind1.extend(&elter2[r1..r2]);
        kind1.extend(&elter1[r2..vlen]);
        kinder.push(kind1);
    }
    kinder
}

fn ordnungsrekombination(eltern :&mut Vec<Vec<usize>>, anzahl_kinder :usize) -> Vec<Vec<usize>> {
    let vlen = eltern[0].len();
    let elen = eltern.len();

    (0..anzahl_kinder).into_par_iter().map(|_a| {
        let mut kind :Vec<usize> = Vec::new();
        let mut rng = rand::thread_rng();
        let r1 = rng.gen_range(0, elen);
        let r2 = rng.gen_range(0, elen);
        let elter1 = &eltern[r1];
        let elter2 = &eltern[r2];
        let j = rng.gen_range(0, vlen);

        for i in 0..j {
            kind.push(elter1[i]);
        }

        for i in 0..vlen {
            if 0 == match kind.iter().find(|&&x| x == elter2[i]) {
                Some(_x) => 1,
                None => 0,
            } {
                kind.push(elter2[i]);
            }
        }
        kind
    }).collect()
}

// buggy
fn ein_punkt_crossover(eltern :&mut Vec<Vec<usize>>, anzahl_kinder :usize) -> Vec<Vec<usize>> {
    let mut max = anzahl_kinder;
    let vlen = eltern[0].len();
    let elen = eltern.len();
    let mut kinder :Vec<Vec<usize>>= Vec::new();
    let mut rng = rand::thread_rng();

    if max % 2 == 0 {
        while max > 0 {
            let r = rng.gen_range(0, vlen);
            let elter1 = &eltern[rng.gen_range(0, elen)];
            let elter2 = &eltern[rng.gen_range(0, elen)];
            let mut kind1 :Vec<usize> = Vec::new();
            let mut kind2 :Vec<usize> = Vec::new();

            kind1.extend(&elter1[0..r]);
            kind1.extend(&elter2[r..vlen]);
            kind2.extend(&elter2[0..r]);
            kind2.extend(&elter1[r..vlen]);
            kinder.push(kind1);
            kinder.push(kind2);
            max -= 2;
        }
    } else {
        while max != 1 {
            let r = rng.gen_range(0, vlen);
            let elter1 = &eltern[rng.gen_range(0, elen)];
            let elter2 = &eltern[rng.gen_range(0, elen)];
            let mut kind1 :Vec<usize> = Vec::new();
            let mut kind2 :Vec<usize> = Vec::new();

            kind1.extend(&elter1[0..r]);
            kind1.extend(&elter2[r..vlen]);
            kind2.extend(&elter2[0..r]);
            kind2.extend(&elter1[r..vlen]);
            kinder.push(kind1);
            kinder.push(kind2);
            max -= 2;
        }
        let r = rng.gen_range(0, elen);
        let elter1 = &eltern[rng.gen_range(0, elen)];
        let elter2 = &eltern[rng.gen_range(0, elen)];
        let mut kind1 :Vec<usize> = Vec::new();

        kind1.extend(&elter1[0..r]);
        kind1.extend(&elter2[r..vlen]);
        kinder.push(kind1);
    }
    kinder
}

/// Umweltselektion
fn turnierselektion(bewertungen :&Vec<i64>, q :usize) -> Vec<usize> {
    let mut rng = rand::thread_rng();
    let mut turnierergebnisse :Vec<(usize, usize)> = Vec::new();
    let mut index :Vec<usize> = Vec::new();

    for i in 0..bewertungen.len() {
        let mut siege = 0;
        for _j in 2..q {
            let r = rng.gen_range(0, bewertungen.len());
            if bewertungen[i] < bewertungen[r] {
                siege += 1;
            }
        }
        turnierergebnisse.push((siege, i));
    }

    turnierergebnisse.par_sort();   // Reihenfolge nach erstem Element
    turnierergebnisse.reverse();    // max zuerst
    // entferne Anzahl der Siege aber behalte den Index
    for i in 0..turnierergebnisse.len() {
        index.push(turnierergebnisse[i].1);
    }
    index
}

/// wähle Eltern nach der Wahrscheinlichkeit ihrer Bewertungen aus
fn selektiere_eltern(individuen: &Vec<Vec<usize>>, bewertungen :&Vec<i64>, anzahl :usize, art :&str) -> Vec<Vec<usize>> {
    //entferne Offset bei Bewertungen und invertiere für Elternselektion, da minProb
    let max_bewertung = match bewertungen.par_iter().max() {
        Some(max) => *max as u64,
        None => panic!("unmöglich: keine maximale Bewertung gefunden"),
    };
    let io_bewertungen = bewertungen.par_iter().map(|&b| (max_bewertung as i64) - b).collect::<Vec<i64>>();
    let eltern :Vec<Vec<usize>>;

    match art {
        "stochastisch" => eltern = stochastisch_universelles_sampling(individuen, &io_bewertungen, anzahl),
        "fitnessproportional" => eltern = fitness_proportionale_selektion(individuen, &io_bewertungen, anzahl),
        _ => eltern = fitness_proportionale_selektion(individuen, &io_bewertungen, anzahl),
    }
    eltern
}

fn stochastisch_universelles_sampling(individuen: &Vec<Vec<usize>>, bewertungen :&Vec<i64>, anzahl :usize) -> Vec<Vec<usize>> {
    // sampling benötigt f64
    let f64bewertungen = bewertungen.par_iter().map(|&b| b as f64 ).collect::<Vec<f64>>();
    // treffe Auswahl
    let eltern :Vec<&Vec<usize>> = random_choice().random_choice_f64(&individuen, &f64bewertungen, anzahl);
    // Umbau, damit Typen stimmen
    eltern.into_par_iter().map(|s| s.to_vec()).collect()
}

fn fitness_proportionale_selektion(individuen: &Vec<Vec<usize>>, bewertungen :&Vec<i64>, anzahl :usize) -> Vec<Vec<usize>> {
    let mut rng = rand::thread_rng();
    let mut summe :Vec<i64> = Vec::new();
    let mut eltern :Vec<Vec<usize>> = Vec::new();
    let len = bewertungen.len();

    summe.push(bewertungen[0]);
    for i in 1..len {
        let vs = summe[i-1];
        //print!("vs: {} ", vs);
        summe.push(vs + bewertungen[i]);
    }

    if summe[len-1] == 0 {
        // falls alle Bewertungen gleich groß sind
        for i in 0..anzahl {
            let j = i % len; // falls anzahl > vorhandene Individuen (sollte nicht eintreten)
            eltern.push(individuen[j].to_vec());
        }
    } else {
        for _i in 0..anzahl {
            let mut j = 0;
            let u = rng.gen_range(0, summe[len - 1]);

            while summe[j] < u {
                j += 1;
            }
            eltern.push(individuen[j].to_vec());
        }
    }
    eltern
}

/// generiert alle Individuen
fn initialisiere_individuen(p :&Problem, anzahl_individuen :usize, individuen :&mut Vec<Vec<usize>>) {
    let mut rng = thread_rng();

    for _i in 0..anzahl_individuen {
        let mut individuum = (0..p.zeiten.len() as usize).collect::<Vec<usize>>();
        rng.shuffle(&mut individuum);
        individuen.push(individuum);
    }
}

fn mutiere(art :&str, individuum :&mut Vec<usize>, mutationswahrscheinlichkeit :f64) {
    let r = rand::thread_rng().gen::<f64>();

    if r < mutationswahrscheinlichkeit {
        match art {
            "vertausche" => vertauschende_mutation(individuum),
            "invertiere" => invertierende_mutation(individuum),
            "verschiebe" => verschiebende_mutation(individuum),
            "mische" => mischende_mutation(individuum),
            _ => vertauschende_mutation(individuum),
        }
    }
}

fn vertauschende_mutation(individuum :&mut Vec<usize>) {
    let l = individuum.len();
    individuum.swap(rand::thread_rng().gen_range(0, l),
                    rand::thread_rng().gen_range(0, l));
}

fn invertierende_mutation(individuum :&mut Vec<usize>) {
    let mut r1 = rand::thread_rng().gen_range(0, individuum.len());
    let mut r2 = rand::thread_rng().gen_range(0, individuum.len());

    if r1 > r2 {
        mem::swap(&mut r1, &mut r2);
    }
    individuum[r1..r2].reverse();
}

fn verschiebende_mutation(individuum :&mut Vec<usize>) {
    let mut rng = rand::thread_rng();
    let r1 = rng.gen_range(0, individuum.len());
    let r2 = rng.gen_range(0, individuum.len());
    let element = individuum.remove(r1);
    individuum.insert(r2, element);
}

fn mischende_mutation(individuum :&mut Vec<usize>) {
    let mut rng = rand::thread_rng();
    let mut r1 = rng.gen_range(0, individuum.len());
    let mut r2 = rng.gen_range(0, individuum.len());

    if r1 > r2 {
        mem::swap(&mut r1, &mut r2);
    }
    rng.shuffle( &mut individuum[r1..r2]);
}

/// bewerte alle Individuen und gebe das beste zurück
fn bewerte_individuen(problem :&Problem, individuen :&Vec<Vec<usize>>) -> Vec<i64> {
    individuen.par_iter().map(|i| bewerte_individuum(&problem, &i)).collect()
}

/// bewertet ein Individuum und gibt die ∑ der Strafen zurück
fn bewerte_individuum(problem :&Problem, permutation :&Vec<usize>) -> i64 {
    let mut strafen_summe : i64 = 0; // Summe der Strafen

    for j in 0..permutation.len() {
        let mut zeit_summe=0;

        for i in 0..j+1 {
            zeit_summe += problem.zeiten[permutation[i as usize] as usize];
        }
        if zeit_summe > problem.fristen[permutation[j as usize] as usize] {
            strafen_summe += problem.strafen[permutation[j as usize] as usize];
        }
    }
    strafen_summe
}

fn durchschnittliche_bewertung(bewertungen :&Vec<i64>) -> f64 {
    let s :i64 = bewertungen.par_iter().sum();
    s as f64 / bewertungen.len() as f64
}

/// finde den Index des besten Individuums und dessen Bewertung
fn finde_bestes_individuum(bewertungen :&Vec<i64>) -> (usize, i64) {
    //let mut min :i64 = std::i64::MAX;
    //let mut bester_index = 0;

    match bewertungen.par_iter().enumerate().min() {
        Some((i,b)) => (i, *b),
        None => (0, std::i64::MAX),
    }
}

fn drucke_individuen(inds :&Vec<Vec<usize>>) {
    for i in inds {
        drucke_individuum(i);
    }
}

fn drucke_individuum(i :&Vec<usize>) {
    println!("{:?}", i);
}

fn drucke_phaenotyp(problem :&Problem, genotyp :&Vec<usize>) {
    print!("Zeit  : ");
    for g in genotyp {
        print!("{} ", problem.zeiten[*g]);
    }
    println!("");

    print!("Frist : ");
    for g in genotyp {
        print!("{} ", problem.fristen[*g]);
    }
    println!("");

    print!("Strafe: ");
    for g in genotyp {
        print!("{} ", problem.strafen[*g]);
    }
    println!("");
}