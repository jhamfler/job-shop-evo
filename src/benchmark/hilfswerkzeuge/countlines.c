int countlines(char * filename);

int countlines(char *filename) {
	FILE *fp = fopen(filename,"r");
	char c=0;
	int lines=0;

	if (fp == NULL) return 0;
	while ((c = fgetc(fp)) != EOF) if (c=='\n') lines++;

	fclose(fp);
	return lines;
}
